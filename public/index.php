<?php

/* Load external routes file */
require_once '../vendor/autoload.php';
require_once '../router.php';

use Pecee\SimpleRouter\SimpleRouter;

/**
 * The default namespace for route-callbacks, so we don't have to specify it each time.
 * Can be overwritten by using the namespace config option on your routes.
 */

//on parametre le router pour avoir comme namespace par defaut les controllers
SimpleRouter::setDefaultNamespace('\Controllers');

// Start the routing
SimpleRouter::start();