<?php
//controller par defaut
namespace Controllers;

    class Controller {

    function getTwig(){
        require_once '../vendor/autoload.php';
        $loader = new \Twig_Loader_Filesystem('../Views');
        $twig = new \Twig_Environment($loader);
        //instancier twig + ajout d'une variable global
        $twig->addGlobal('session', $_SESSION);
        return $twig;
    }
};
?>